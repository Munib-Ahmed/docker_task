FROM ubuntu:bionic

RUN apt-get update \
	&& apt-get install -y cmake \
	&& apt-get install -y make \
	&& apt-get install -y python3\
	&& apt-get install -y libboost-all-dev\
	&& apt-get install -y g++ \
	&& apt-get install -y gcc \
	&& apt-get install -y git
	
WORKDIR /usr/src
