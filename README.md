# Docker_task



## Getting started

this is a simple project, in which we will be using docker with ubuntu as the base image to run a c++ code in our container.

- clone this project into your system, using the following command;
```bash
git clone https://gitlab.com/Munib-Ahmed/docker_task.git
``` 


Enter the cloned Directory, with

```bash
cd docker_task
```

we can run our contain with or without docker-compose, let's look into both one by one. Firstly, without docker-compose;
```bash
docker build -t the_task_image .
sudo docker run --name the_task_container -it -v /home/emumba/Desktop/Docker/docker_1/docker_task/program:/usr/src the_task_image
(i have added the location address according to my directory. please make sure you are adding yours correctly)
```

Now, with docker-compse;
```
docker-compose build

docker-compose up -d

docker exec -ti the_task_conatiner /bin/bash

```




From now on the process is same for both i-e;
Now, we need to create a directory typically named as "build"
```bash
mkdir build
```

navigate to build directory, 
```bash
cd build
```

## How to Run:
simply navigate to the build directory and enter the following commands in terminal:
```bash
cmake ..
cmake --build .
./docker 
```

